Dear CRAN maintainers,

Ok, the {codebook} vignette should now work again!

Thank you for all your work on CRAN!

Kind regards,

Gjalt-Jorn
